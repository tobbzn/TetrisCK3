﻿on_after_gui_dummy_male = {
	on_actions = {
		on_after_gui_dummy_male_tetris
	}
}
on_after_gui_dummy_male_tetris = {
	effect = {
		add_sidebar_button = {NAME = tetris}
		every_player = {
			start_tetris = yes
		}
	}
}